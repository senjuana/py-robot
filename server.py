#!/usr/bin/python3
from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from json import dumps
import random

db_connect = create_engine('sqlite:///mydatabase.db')
app = Flask(__name__)
api = Api(app)

@app.route("/")
def hello():
    return "Hello World!"


class Robot(Resource):
    def get(self):
        conn = db_connect.connect() # connect to database
        query = conn.execute("select * from robot") # This line performs query and returns json result
        result = [dict(zip(tuple (query.keys()) ,i)) for i in query.cursor]
        print(result)
        return jsonify(result)

    def post(self):
        conn = db_connect.connect()
        print(request.json)
        ids = "ag-"
        method = request.json['method']
        name = request.json['name']
        ids+=str(random.randint(1,101))
        query = conn.execute("insert into robot values('{0}','{1}','{2}')".format(ids,name,method))
        return {'status':'success'}

class UpdateR(Resource):
    def post(self):
        conn = db_connect.connect()
        ids = request.json['id']
        method = request.json['method']
        name = request.json['name']
        query = conn.execute("update robot set id='{0}',name='{1}',method='{2}' where id='{0}'".format(ids,name,method))
        return {'status':'success'}
   
class DeleteR(Resource):
    def get(self, robot_id):
        conn = db_connect.connect()
        query = conn.execute("delete  from robot where id='"+robot_id+"'")
        return {'status':'success'}

api.add_resource(Robot, '/robots') # Route_1
api.add_resource(UpdateR, '/update') # Route_2
api.add_resource(DeleteR, '/delete/<robot_id>') # Route_3

if __name__ == '__main__':
     app.run(host='0.0.0.0', port=8080)
